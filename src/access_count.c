/************************
access_count.

The sole intention of this simple benchmark is to
be as cache-unfriendly as possible while having an
allocation site which we know the exact number of LLC
cache misses to. With this value, we can calculate
what percentage of accesses are getting sampled by our
PEBS profiling.
************************/

#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

int main(int argc, char **argv) {
  node *root;
  unsigned long num_nodes, node_size, num_threads;
  
  if(argc != 4) {
    fprintf(stderr, "Incorrect number of arguments. Aborting.\n");
    exit(1);
  }
  
  num_nodes = strtoul(argv[1], NULL, 0);
  node_size = strtoul(argv[2], NULL, 0);
  num_threads = strtoul(argv[3], NULL, 0);
  
  root = linked_list_init(num_nodes, node_size);
  linked_list_shuffle(&root);
  linked_list_random_fill(root, node_size, num_threads);
  linked_list_random_access(root, node_size, num_threads);
  linked_list_free(root);
}
